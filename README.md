# mysql-backup

# раскрыть архив и стрим
(делал на локальной машине)
```
/usr/local/opt/openssl/bin/openssl des3 -salt -k "password" -d -in backup.xbstream.gz-4560-0d8b3a.des3 -out backup.xbstream.gz
gzip -d backup.xbstream.gz
```


# установка пакетов перконы и распаковка бекапа
```
apt-get update -y && apt-get install -y wget lsb-release curl && apt-get clean all
wget https://repo.percona.com/apt/percona-release_latest.$(lsb_release -sc)_all.deb
dpkg -i percona-release_latest.$(lsb_release -sc)_all.deb
apt-get update -y && apt-get install percona-xtrabackup-80

xbstream -x < /opt/data/backup.xbstream

mysql -u user -p
mysql> use db;
mysql> alter table articles discard tablespace;

cp /tmp/otus/articles.* /var/lib/mysql/db/
chown mysql:mysql /var/lib/mysql/db/articles.*

mysql -u user -p
mysql> use db;
mysql> alter table articles impoert tablespace;

mysql> select * from articles;
+----+-----------------------+------------------------------------------+
| id | title                 | body                                     |
+----+-----------------------+------------------------------------------+
|  1 | MySQL Tutorial        | DBMS stands for DataBase ...             |
|  2 | How To Use MySQL Well | After you went through a ...             |
|  3 | Optimizing MySQL      | In this tutorial we will show ...        |
|  4 | 1001 MySQL Tricks     | 1. Never run mysqld as root. 2. ...      |
|  5 | MySQL vs. YourSQL     | In the following database comparison ... |
|  6 | MySQL Security        | When configured properly, MySQL ...      |
|  7 | Oracle configuration  | Beginning guide                          |
|  8 | RDBMS course          | Trash and hell                           |
|  9 | RDBMS course2         | Trash and hell2                          |
| 10 | RDBMS course3         | Trash and hell3                          |
| 11 | RDBMS course10        | Trash and hell10                         |
+----+-----------------------+------------------------------------------+
11 rows in set (0.00 sec)
```
